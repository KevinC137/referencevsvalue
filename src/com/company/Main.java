package com.company;

import java.util.Arrays;
// Comment Blah, blah, blah
public class Main {

    public static void main(String[] args) {

        int myInt = 10;
        int otherInt = myInt;

        System.out.println("myInt = " + myInt);
        System.out.println("otherInt = " + otherInt);

        otherInt++;

        System.out.println("myInt = " + myInt);
        System.out.println("otherInt = " + otherInt);

        int[] myIntArray = new int[5];
        int[] otherIntArray = myIntArray;

        System.out.println("myIntArray " + Arrays.toString(myIntArray));
        System.out.println("otherIntArray " + Arrays.toString(otherIntArray));

        otherIntArray[0] = 1;

        System.out.println("myIntArray " + Arrays.toString(myIntArray));
        System.out.println("otherIntArray " + Arrays.toString(otherIntArray));

        otherIntArray = new int[] {6, 7, 8, 9, 10};
        modArray(myIntArray);

        System.out.println("myIntArray " + Arrays.toString(myIntArray));
        System.out.println("otherIntArray " + Arrays.toString(otherIntArray));
    }

    private static void modArray(int[] array) {
        array[0] = 2;
        array = new int[] {1, 2, 3, 4, 5};
    }
}
